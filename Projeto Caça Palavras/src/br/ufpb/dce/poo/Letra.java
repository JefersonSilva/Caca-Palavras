package br.ufpb.dce.poo;

import java.io.Serializable;

public class Letra implements Serializable {
	
	private static final long serialVersionUID = 3183186266700568498L;
	
	private char letra;
	private byte posLinha;
	private byte posColuna;
	private boolean mudou;
	
	public Letra(){
		this(' ',(byte)0,(byte)0);
	}
	public Letra(char letra, byte posLinha, byte posColuna) {
		this.letra = letra;
		this.posLinha = posLinha;
		this.posColuna = posColuna;
		this.mudou = false;
	}
	public char getLetra() {
		return letra;
	}
	public void setLetra(char letra){
		this.letra=letra;
		this.mudou = true;
	}
	public byte getPosLinha() {
		return posLinha;
	}
	public void setPosLinha(byte posLinha) {
		this.posLinha = posLinha;
	}
	public byte getPosColuna() {
		return posColuna;
	}
	public void setPosColuna(byte posColuna) {
		this.posColuna = posColuna;
	}
	
	
	public boolean isMudou() {
		return mudou;
	}
	public void setMudou(boolean mudou) {
		this.mudou = mudou;
	}
	@Override
	public String toString() {
		return "["+letra+"]";
	}
}
