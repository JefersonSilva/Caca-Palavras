package br.ufpb.dce.poo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GavadorDeJogo {
	
	public void gravarDados(Matriz matrizJogo) throws FileNotFoundException, IOException{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("matriz.txt"));
		output.writeObject(matrizJogo);
		output.close();
	}
	public Matriz leDados() throws FileNotFoundException, IOException, ClassNotFoundException{
		Matriz matriz = new Matriz();
		ObjectInputStream input = new ObjectInputStream(new FileInputStream("matriz.txt"));
		matriz = (Matriz) input.readObject();
		input.close();
		return matriz;
	}

}
