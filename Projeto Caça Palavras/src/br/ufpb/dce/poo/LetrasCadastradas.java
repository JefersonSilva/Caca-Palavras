package br.ufpb.dce.poo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class LetrasCadastradas{
	private Map<String,ArrayList<Letra>> palavrasCadastradas;
	
	public LetrasCadastradas(){
		this(new HashMap<String,ArrayList<Letra>>());
	}
	
	
	public LetrasCadastradas(HashMap<String,ArrayList<Letra>> letrasDaPalavra) {
		this.palavrasCadastradas = letrasDaPalavra;
	}
	
	public void adicionaPalavrasCadastradas(String palavra,ArrayList<Letra> listaLetras){
		this.palavrasCadastradas.put(palavra, listaLetras);
	}
	
	public Letra getLetrasCadastradasDaPalavra(String palavra){
		Letra letraRetorno = new Letra();
		for(Letra l : this.palavrasCadastradas.get(palavra)){
				letraRetorno = l;
		}
		return letraRetorno;
	}

	public ArrayList<Letra> getListaDeLetrasDaPalavraCadastrada(String palavra){
		return palavrasCadastradas.get(palavra);
	}


	public void setLetrasDaPalavra(Map<String, ArrayList<Letra>> letrasDaPalavra) {
		this.palavrasCadastradas = letrasDaPalavra;
	}
	
}
