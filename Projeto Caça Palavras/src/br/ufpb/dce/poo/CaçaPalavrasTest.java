package br.ufpb.dce.poo;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

public class CaçaPalavrasTest {

	@Test
	public void Test() {
		GerenciadorDeJogo g = new GerenciadorDeJogo();
		assertNotNull(g);
		g.getNivelJogo().nivelFacil();
		byte numeroLinhas = (byte) g.getNivelJogo().getMatriz().getLetras().length;
		byte numeroColunas = (byte) g.getNivelJogo().getMatriz().getLetras().length;
		byte linhas = 0;
		byte colunas = 0;
		for(byte i = 0; i <= g.getNivelJogo().getMatriz().getLetras().length; i++){
			for(byte j = 0; j <= g.getNivelJogo().getMatriz().getLetras().length;j++){
				linhas = i;
				colunas = j;
			}
		}
		assertEquals(numeroLinhas,linhas);
		assertEquals(numeroColunas,colunas);
		g.carregaTabuleiroComLetrasAleatorias();
		Letra aux = null;
		for(byte i = 0; i < g.getNivelJogo().getMatriz().getLetras().length; i++){
			for(byte j = 0; j < g.getNivelJogo().getMatriz().getLetras().length;j++){
				aux = g.getNivelJogo().getMatriz().getLetras()[i][j];
				assertNotNull(aux);
			}
		}
		try {
			String palavra = new String("HERANÇA");
			g.adicionarPalavraHorizontal(palavra);
			char vetorPalavra[] = palavra.toCharArray();
			boolean palavraCadastrada = false;
			for(byte i = 0; i < vetorPalavra.length; i++){
				if(vetorPalavra[i] == g.getLetrasDasPalavrasCadastradas().getListaDeLetrasDaPalavraCadastrada(palavra).get(i).getLetra()){
					palavraCadastrada = true;
				}else{
					palavraCadastrada = false;
				}
			}
			assertTrue(palavraCadastrada);
		} catch (AdicionaPalavrasException e) {
			e.getMessage();
		}
		try {
			String palavra = new String("PROGRAMAÇÃO");
			g.adicionarPalavraVertical(palavra);
			char vetorPalavra[] = palavra.toCharArray();
			boolean palavraCadastrada = false;
			for(byte i = 0; i < vetorPalavra.length; i++){
				if(vetorPalavra[i] == g.getLetrasDasPalavrasCadastradas().getListaDeLetrasDaPalavraCadastrada(palavra).get(i).getLetra()){
					palavraCadastrada = true;
				}else{
					palavraCadastrada = false;
				}
			}
			assertTrue(palavraCadastrada);
		
		} catch (AdicionaPalavrasException e) {
			e.printStackTrace();
		}
		
		try {
			g.getGravador().gravarDados(g.getNivelJogo().getMatriz());
			try {
				Matriz matriz = null;
				matriz = g.getGravador().leDados();
				assertNotNull(matriz);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

