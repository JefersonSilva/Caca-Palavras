package br.ufpb.dce.poo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GerenciadorDeJogo{

	private Nivel nivelJogo;
	private LetrasCadastradas letrasDasPalavrasCadastradas;
	private GavadorDeJogo gravador;

	public GerenciadorDeJogo(){
		this.nivelJogo= new Nivel();
		this.letrasDasPalavrasCadastradas = new LetrasCadastradas();
		this.gravador = new GavadorDeJogo();
	}

	public boolean verificaConflitoHorizontal(char palavraVetor [],byte posLinha, byte posColuna){
		boolean conflito = false;
		for(byte i = 0; i < palavraVetor.length; i++){
			if(!(this.nivelJogo.getMatriz().getLetras()[posLinha][posColuna].isMudou())){
				posColuna++;
				conflito = false;
			}else{
				conflito = true;
			}
		}
		return conflito;
	}
	public void adicionarPalavraHorizontal(String palavra) throws AdicionaPalavrasException{
		char palavraVetor [] = palavra.toCharArray();
		if(palavraVetor.length >= this.nivelJogo.getMatriz().getLetras().length){
			throw new AdicionaPalavrasException("N�o foi possivel adicionar a palavra");
		}else{
			byte linha = (byte) (Math.random() * this.nivelJogo.getMatriz().getLetras().length-1);
			byte posInicialNaLinha = (byte) (Math.random() * ((this.nivelJogo.getMatriz().getLetras().length) - palavraVetor.length));
			while(this.verificaConflitoHorizontal(palavraVetor, linha, posInicialNaLinha)){
				linha = (byte) (Math.random() * this.nivelJogo.getMatriz().getLetras().length-1);
				posInicialNaLinha = (byte) (Math.random() * ((this.nivelJogo.getMatriz().getLetras().length) - palavraVetor.length));
			}

			ArrayList<Letra> letrasDaPalavra = new ArrayList<Letra>();
			for(byte i=0; i<palavraVetor.length;i++){
				this.nivelJogo.getMatriz().getLetras()[linha][posInicialNaLinha].setLetra(palavraVetor[i]);
				letrasDaPalavra.add(this.nivelJogo.getMatriz().getLetras()[linha][posInicialNaLinha]);
				this.letrasDasPalavrasCadastradas.adicionaPalavrasCadastradas(palavra,letrasDaPalavra);
				posInicialNaLinha++;			
			}
		}
	}
	public boolean verificaConflitoVertical(char palavraVetor [],byte posLinha, byte posColuna){
		boolean conflito = false;
		for(byte i = 0; i < palavraVetor.length; i++){
			if(!(this.nivelJogo.getMatriz().getLetras()[posColuna][posLinha].isMudou())){
				posColuna++;
				conflito = false;
			}else{
				conflito = true;
			}			
		}
		return conflito;
	}
	public void adicionarPalavraVertical(String palavra)throws AdicionaPalavrasException{
		char palavraVetor [] = palavra.toCharArray();
		if(palavraVetor.length >= this.nivelJogo.getMatriz().getLetras().length){
			throw new AdicionaPalavrasException("N�o foi possivel adicionar a palavra");
		}else{
			byte coluna = (byte) (Math.random() * this.nivelJogo.getMatriz().getLetras().length-1);
			byte posInicialNaColuna = (byte) (Math.random() * ((this.nivelJogo.getMatriz().getLetras().length) - palavraVetor.length));
			while(this.verificaConflitoVertical(palavraVetor, coluna, posInicialNaColuna)){
				coluna = (byte) (Math.random() * this.nivelJogo.getMatriz().getLetras().length-1);
				posInicialNaColuna = (byte) (Math.random() * ((this.nivelJogo.getMatriz().getLetras().length) - palavraVetor.length));
			}
			ArrayList<Letra> letrasDaPalavra = new ArrayList<Letra>();
			for(byte i=0; i<palavraVetor.length;i++){
				this.nivelJogo.getMatriz().getLetras()[posInicialNaColuna][coluna].setLetra(palavraVetor[i]);
				letrasDaPalavra.add(this.nivelJogo.getMatriz().getLetras()[posInicialNaColuna][coluna]);
				this.letrasDasPalavrasCadastradas.adicionaPalavrasCadastradas(palavra,letrasDaPalavra);
				posInicialNaColuna++;
			}
		}
	}

	public void carregaTabuleiroComLetrasAleatorias(){
		Random r = new Random();
		for(byte i = 0; i < this.nivelJogo.getMatriz().getLetras().length; i++ ){
			for(byte j = 0; j< this.nivelJogo.getMatriz().getLetras().length; j++){
				char letra = (char) (r.nextInt(26) + 'a');
				this.nivelJogo.getMatriz().getLetras()[i][j] = new Letra(letra,i,j);
			}
		}
	}

	public void mostraMatriz() {
		for(int i = 0; i < this.nivelJogo.getMatriz().getLetras().length;i++){
			for(int j = 0; j < this.nivelJogo.getMatriz().getLetras().length;j++){
				System.out.print(this.nivelJogo.getMatriz().getLetras()[i][j]);				
				if(j == this.nivelJogo.getMatriz().getLetras().length-1){
					System.out.printf("\r");
				}
			}
		}		
	}

	public Letra [] marcarPalavra (byte linha , byte coluna,byte tamanho,Letra tabuleiro[][]){

		Letra LetrasInicial = tabuleiro [linha][coluna];
		Letra []  LetrasMarcadas = new Letra[tamanho];

		for(byte i = 0;i<tamanho;i++){
			if (LetrasInicial.getPosLinha() == tabuleiro [linha][coluna+1].getPosLinha()){
				Letra q = LetrasInicial;
				q = tabuleiro[linha][coluna];
				coluna++;
				LetrasMarcadas [i] = q;
			}else{
				Letra q = LetrasInicial;
				q = tabuleiro[linha][coluna];
				linha++;
				LetrasMarcadas [i] = q;	
			}
		}
		return LetrasMarcadas;

	}

	public boolean checaPalavraCorreta(String palavra,Letra [] arrayLetrasDapalavra){
		byte temPalavra = 1;
		for(int i = 0; i<arrayLetrasDapalavra.length;i++){
			for (Letra l : this.letrasDasPalavrasCadastradas.getListaDeLetrasDaPalavraCadastrada(palavra)){
				if (arrayLetrasDapalavra[i]==l){
					temPalavra++;
				}
			}
			i++;
		}
		System.out.println("tem palavra "+temPalavra);
		if(temPalavra==arrayLetrasDapalavra.length){
			System.out.println("tem a palavra sim");
			return true;

		}else
			System.out.println("realmente nao tem");
		return false;
	}
	public void sairDojogo(){
		try {
			this.gravador.gravarDados(this.nivelJogo.getMatriz());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void recuperarRodada(){
		try {
			this.nivelJogo.setMatriz(this.gravador.leDados());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Nivel getNivelJogo() {
		return nivelJogo;
	}

	public void setNivelJogo(Nivel nivelJogo) {
		this.nivelJogo = nivelJogo;
	}

	public LetrasCadastradas getLetrasDasPalavrasCadastradas() {
		return this.letrasDasPalavrasCadastradas;
	}

	public void setLetrasDasPalavrasCadastradas(LetrasCadastradas letrasDasPalavrasCadastradas) {
		this.letrasDasPalavrasCadastradas = letrasDasPalavrasCadastradas;
	}

	public GavadorDeJogo getGravador() {
		return this.gravador;
	}

	public void setGravador(GavadorDeJogo gravador) {
		this.gravador = gravador;
	}


}
