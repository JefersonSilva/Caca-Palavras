package br.ufpb.dce.poo.logica;

import java.util.ArrayList;
import java.util.Random;

public class GerenciadorDeNivel {

	private Nivel nivel;
	private VerificadorDeConflito conflito;

	public GerenciadorDeNivel(){
		this.nivel = new Nivel();
		this.conflito = new VerificadorDeConflito();
	}

	public void adicionarPalavraHorizontal(String palavra) throws AdicionaPalavrasException{
		char palavraVetor [] = palavra.toCharArray();
		if(palavraVetor.length >= this.nivel.getMatriz().getLetras().length){
			throw new AdicionaPalavrasException("N�o foi possivel adicionar a palavra");
		}else{
			byte linha = (byte) (Math.random() * this.nivel.getMatriz().getLetras().length-1);
			byte coluna = (byte) (Math.random() * ((this.nivel.getMatriz().getLetras().length) - palavraVetor.length));
			while(this.conflito.verificaConflitoHorizontal(this.nivel.getMatriz(),palavraVetor, linha, coluna)){
				linha = (byte) (Math.random() * this.nivel.getMatriz().getLetras().length-1);
				coluna = (byte) (Math.random() * ((this.nivel.getMatriz().getLetras().length) - palavraVetor.length));
			}

			ArrayList<Letra> letrasDaPalavra = new ArrayList<Letra>();
			for(byte i=0; i<palavraVetor.length;i++){
				this.nivel.getMatriz().getLetras()[linha][coluna].setLetra(palavraVetor[i]);
				letrasDaPalavra.add(this.nivel.getMatriz().getLetras()[linha][coluna]);
				this.nivel.getPalavrasDoNivel().adicionaPalavrasCadastradas(palavra,letrasDaPalavra);
				coluna++;			
			}
		}
	}

	public void adicionarPalavraVertical(String palavra)throws AdicionaPalavrasException{
		char palavraVetor [] = palavra.toCharArray();
		if(palavraVetor.length >= this.nivel.getMatriz().getLetras().length){
			throw new AdicionaPalavrasException("N�o foi possivel adicionar a palavra");
		}else{
			byte coluna = (byte) (Math.random() * this.nivel.getMatriz().getLetras().length-1);
			byte linha = (byte) (Math.random() * ((this.nivel.getMatriz().getLetras().length) - palavraVetor.length));
			
			while(this.conflito.verificaConflitoVertical(this.nivel.getMatriz(),palavraVetor,coluna,linha)){
				coluna = (byte) (Math.random() * this.nivel.getMatriz().getLetras().length-1);
				linha = (byte) (Math.random() * ((this.nivel.getMatriz().getLetras().length) - palavraVetor.length));
			}
			
			ArrayList<Letra> letrasDaPalavra = new ArrayList<Letra>();
			for(byte i=0; i<palavraVetor.length;i++){
				this.nivel.getMatriz().getLetras()[linha][coluna].setLetra(palavraVetor[i]);
				letrasDaPalavra.add(this.nivel.getMatriz().getLetras()[linha][coluna]);
				this.nivel.getPalavrasDoNivel().adicionaPalavrasCadastradas(palavra,letrasDaPalavra);
				linha++;
			}
		}
	}

	public void carregaMatrizComLetrasAleatorias(){
		Random r = new Random();
		for(byte i = 0; i < this.nivel.getMatriz().getLetras().length; i++ ){
			for(byte j = 0; j< this.nivel.getMatriz().getLetras().length; j++){
				char letra = (char) (r.nextInt(26) + 'A');
				this.nivel.getMatriz().getLetras()[i][j] = new Letra(letra,i,j);
			}
		}
	}
	
	public void mostraMatriz() {
		for(int i = 0; i < this.nivel.getMatriz().getLetras().length;i++){
			for(int j = 0; j < this.nivel.getMatriz().getLetras().length;j++){
				System.out.print(this.nivel.getMatriz().getLetras()[i][j]);				
				if(j == this.nivel.getMatriz().getLetras().length-1){
					System.out.printf("\r");
				}
			}
		}
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public VerificadorDeConflito getConflito() {
		return conflito;
	}

	public void setConflito(VerificadorDeConflito conflito) {
		this.conflito = conflito;
	}
	
}
