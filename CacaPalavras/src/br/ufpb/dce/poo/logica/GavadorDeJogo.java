package br.ufpb.dce.poo.logica;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GavadorDeJogo {

	public void gravarRodada(Nivel NivelJogo) throws FileNotFoundException, IOException{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("rodada.obj"));
		output.writeObject(NivelJogo);
		output.close();
	}

	public Nivel lerRodada() throws FileNotFoundException, IOException, ClassNotFoundException{
		Nivel nivel = new Nivel();
		ObjectInputStream input = new ObjectInputStream(new FileInputStream("rodada.obj"));
		nivel = (Nivel) input.readObject();
		input.close();
		return nivel;
	}
	
	
	
	
}
