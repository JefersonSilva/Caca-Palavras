package br.ufpb.dce.poo.logica;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PalavrasCadastradas implements Serializable{
	
	
	private static final long serialVersionUID = -6792946781845870865L;
	
	private Map<String,ArrayList<Letra>> palavrasCadastradas;
	
	public PalavrasCadastradas(){
		this(new HashMap<String,ArrayList<Letra>>());
	}
	
	
	public PalavrasCadastradas(HashMap<String,ArrayList<Letra>> letrasDaPalavra) {
		this.palavrasCadastradas = letrasDaPalavra;
	}
	
	public void adicionaPalavrasCadastradas(String palavra,ArrayList<Letra> listaDeLetras){
		this.palavrasCadastradas.put(palavra, listaDeLetras);
	}
	
	public Letra getLetrasCadastradasDaPalavra(String palavra){
		Letra letraRetorno = new Letra();
		for(Letra l : this.palavrasCadastradas.get(palavra)){
				letraRetorno = l;
		}
		return letraRetorno;
	}

	public ArrayList<Letra> getListaDeLetrasDaPalavraCadastrada(String palavra){
		return palavrasCadastradas.get(palavra);
	}


	public void setLetrasDaPalavra(Map<String, ArrayList<Letra>> letrasDaPalavra) {
		this.palavrasCadastradas = letrasDaPalavra;
	}


	public Map<String, ArrayList<Letra>> getPalavrasCadastradas() {
		return palavrasCadastradas;
	}


	public void setPalavrasCadastradas(
			Map<String, ArrayList<Letra>> palavrasCadastradas) {
		this.palavrasCadastradas = palavrasCadastradas;
	}
	
	
	
}
