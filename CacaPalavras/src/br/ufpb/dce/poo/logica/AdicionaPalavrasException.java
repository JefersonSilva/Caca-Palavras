package br.ufpb.dce.poo.logica;


public class AdicionaPalavrasException extends Exception {


	private static final long serialVersionUID = 797763854166550540L;

	public AdicionaPalavrasException(String message){
		super("Erro ao adcionar a palavra!");
	}

}
