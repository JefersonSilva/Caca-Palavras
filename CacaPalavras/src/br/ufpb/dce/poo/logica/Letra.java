package br.ufpb.dce.poo.logica;

import java.io.Serializable;

public class Letra implements Serializable {
	
	private static final long serialVersionUID = 3183186266700568498L;
	
	private char letra;
	private byte linha;
	private byte coluna;
	private boolean cadastrou;
	
	public Letra(){
		this(' ',(byte)0,(byte)0);
	}
	public Letra(char letra, byte posLinha, byte posColuna) {
		this.letra = letra;
		this.linha = posLinha;
		this.coluna = posColuna;
		this.cadastrou = false;
	}
	public char getLetra() {
		return letra;
	}
	public void setLetra(char letra){
		this.letra=letra;
		this.cadastrou = true;
	}
	public byte getLinha() {
		return linha;
	}
	public void setLinha(byte posLinha) {
		this.linha = posLinha;
	}
	public byte getColuna() {
		return coluna;
	}
	public void setColuna(byte posColuna) {
		this.coluna = posColuna;
	}
	
	public boolean isCadastrou() {
		return cadastrou;
	}
	public void setCadastrou(boolean mudou) {
		this.cadastrou = mudou;
	}
	@Override
	public String toString() {
		return "["+letra+"]";
	}
}
