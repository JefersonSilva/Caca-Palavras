package br.ufpb.dce.poo.logica;

public class GerenciadorDeJogo{

	private GerenciadorDeNivel gerenteDeNivel;
	private GavadorDeJogo gravador;

	public GerenciadorDeJogo(){
		this.gerenteDeNivel = new GerenciadorDeNivel();
		this.gravador = new GavadorDeJogo();
	}

	public GerenciadorDeNivel getGerenteDeNivel() {
		return gerenteDeNivel;
	}


	public void setGerenteDeNivel(GerenciadorDeNivel gerenteMatriz) {
		this.gerenteDeNivel = gerenteMatriz;
	}


	public GavadorDeJogo getGravador() {
		return this.gravador;
	}

	public void setGravador(GavadorDeJogo gravador) {
		this.gravador = gravador;
	}


}
